import { createRouter, createWebHistory } from 'vue-router'
import Home from "../views/Home.vue"
import Gallerie from "../views/Gallerie.vue"

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/gallerie',
    name: 'Gallerie',
    component: Gallerie
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
